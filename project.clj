
(defproject ri4 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/tools.logging "0.2.6"]
                 [org.slf4j/slf4j-log4j12 "1.7.5"]
                 [com.google.guava/guava "15.0"]
                 [com.amazonaws/aws-java-sdk "1.6.12"]]
  :profiles {:dev {:dependencies [[junit/junit "4.11"]]}})
