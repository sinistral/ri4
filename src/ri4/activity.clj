
(ns ri4.activity)

(defmacro defactivity
  [name registration-options {:keys [function version] :as entry-point-spec}]
  `(when-not (ns-resolve *ns* '+activities+)
     (def +activities+ )))

(defrecord ActivitySpec [name registration-options])

(defn make-activity-spec
  [name registration-options {:keys [function version] :as entry-point-spec}]
  (map->ActivitySpec {:name name
                      :version version
                      :registration-options registration-options
                      :entry-point entry-point-spec}))
