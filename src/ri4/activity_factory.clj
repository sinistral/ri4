
(ns ri4.activity-factory
  (:import [com.google.common.base
            Strings]
           [com.amazonaws.services.simpleworkflow.flow.common
            FlowConstants]
           [com.amazonaws.services.simpleworkflow.flow.generic
            ActivityImplementation
            ActivityImplementationFactory]
           [com.amazonaws.services.simpleworkflow.flow.worker
            ActivityTypeExecutionOptions
            ActivityTypeRegistrationOptions]
           [com.amazonaws.services.simpleworkflow.model
            ActivityType])
  (:require [ri4.util.bean :refer [default set-fields!]]))

(defn make-activity-type
  [spec]
  (doto (ActivityType.)
    (.withName (:name spec))
    (.withVersion (get-in spec [:entry-point :version]))))

(defn make-activity-registration-options
  [opts]
  (set-fields! (ActivityTypeRegistrationOptions.)
               opts
               {:description
                #(Strings/emptyToNull %)
                :default-task-heartbeat-timeout-seconds
                (default (long FlowConstants/NONE))
                :default-task-schedule-to-close-timeout-seconds
                (default (long FlowConstants/NONE))}))

(defn make-activity-execution-options
  [opts]
  (ActivityTypeExecutionOptions.))

(defn make-activity-implementation
  [spec]
  (proxy [ActivityImplementation]
      []
    (execute [context]
      ((get-in spec [:entry-point :function]) context))
    (getRegistrationOptions []
      (make-activity-registration-options (:registration-options spec)))
    (getExecutionOptions []
      (make-activity-execution-options (:execution-options spec)))))


(defn ^:private make-type-impl-map
  [specs map]
  (if (empty? specs)
    map
    (let [spec (first specs)
          type (make-activity-type spec)
          impl (make-activity-implementation spec)]
      (recur (rest specs) (assoc map type impl)))))

(defn make-activity-implementation-factory
  [specs]
  (let [type-impl-map (make-type-impl-map specs {})]
    (proxy [ActivityImplementationFactory]
        []
      (getActivityTypesToRegister []
        (keys type-impl-map))
      (getActivityImplementation [type]
        (get type-impl-map type)))))
