
(ns ri4.workflow-factory
  (:import [com.google.common.base
            Strings]
           [com.amazonaws.services.simpleworkflow.flow
            JsonDataConverter
            WorkflowTypeRegistrationOptions]
           [com.amazonaws.services.simpleworkflow.flow.generic
            WorkflowDefinition
            WorkflowDefinitionFactory
            WorkflowDefinitionFactoryFactory]
           [com.amazonaws.services.simpleworkflow.model
            ChildPolicy
            WorkflowType])
  (:require [ri4.util.bean :refer [default set-fields!]]))

(defn make-workflow-type
  [spec]
  (doto (WorkflowType.)
    (.withName (:name spec))
    (.withVersion (get-in spec [:entry-point :version]))))

(defn make-workflow-registration-options
  [opts]
  (set-fields! (WorkflowTypeRegistrationOptions.)
               opts
               {:description #(Strings/emptyToNull %)
                :default-task-start-to-close-timeout-seconds (default 30)
                :default-child-policy (default ChildPolicy/TERMINATE)}))

(defn make-workflow-definition
  [spec]
  (proxy [WorkflowDefinition]
      []
    (execute [input]
      (let [convert (or (:data-converter spec)
                        #(.fromData (JsonDataConverter.) %))]
        ((get-in spec [:entry-point :function]) (convert input))))
    (signalReceived [signal-name input]
      nil)
    (getWorkflowState []
      nil)))

(defn make-workflow-definition-factory
  [spec]
  (proxy [WorkflowDefinitionFactory]
      []
    (getWorkflowRegistrationOptions []
      (make-workflow-registration-options (:registration-options spec)))
    (getWorkflowDefinition [_]
      (make-workflow-definition spec))
    (getWorkflowType []
      (make-workflow-type spec))
    (deleteWorkflowDefinition [spec]
      (throw (UnsupportedOperationException.)))))

(defn ^:private make-type-def-map
  [specs map]
  (if (empty? specs)
    map
    (let [factory (make-workflow-definition-factory (first specs))]
      (recur (rest specs) (assoc map (.getWorkflowType factory) factory)))))

(defn make-workflow-definition-factory-factory
  [specs]
  (let [type-def-map (make-type-def-map specs {})]
    (proxy [WorkflowDefinitionFactoryFactory]
        []
      (getWorkflowDefinitionFactory [type]
        (get type-def-map type))
      (getWorkflowTypesToRegister []
        (keys type-def-map)))))
