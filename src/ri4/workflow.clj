
(ns ri4.workflow
  (:import [com.amazonaws.services.simpleworkflow.flow
            DecisionContextProviderImpl]
           [com.amazonaws.services.simpleworkflow.flow.core
            Promise
            Task]))

(def ^:dynamic *decision-context* nil)

(defn decision-context
  []
  (.getDecisionContext (DecisionContextProviderImpl.)))

(defmacro asynchronously [promises & body]
  `(proxy [Task]
       [(into-array Promise ~promises)]
     (doExecute []
       ;; The `*decision-context*` is bound when the task executed (cf.
       ;; `wrap-entry-point-fn`), but Tasks are executed asyncronously,
       ;; in a different thread, and so it must be re-bound before passing
       ;; control to the decider logic.
       (binding [*decision-context* (decision-context)]
         ~@body))))

(defn schedule-activity
  [name version input]
  (let [client (.getActivityClient *decision-context*)]
    (.scheduleActivityTask client name version input)))

(defn create-timer
  [seconds]
  (.createTimer (.getWorkflowClock *decision-context*) seconds))

;; -------------------------------------------------------------------------- ;;

(defrecord WorkflowSpec [name registration-options definition])

(defn ^:private wrap-entry-point-fn
  [f]
  (fn [& args]
    (binding [*decision-context* (decision-context)]
      (apply f args))))

(defn make-workflow-spec
  "`registration-options` correspond to http://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/index.html?com/amazonaws/services/simpleworkflow/flow/generic/WorkflowDefinitionFactory.html"
  [name registration-options {:keys [function version] :as entry-point-spec}]
  (map->WorkflowSpec {:name name
                      :version version
                      :registration-options registration-options
                      :entry-point (assoc entry-point-spec
                                     :function
                                     (wrap-entry-point-fn function))}))
