
(ns ri4.worker.workflow-worker
  (:import [com.amazonaws.services.simpleworkflow.flow.worker
            GenericWorkflowWorker]))

(defn make-worker
  [service domain-name task-list workflow-definition-factory-factory]
  (doto (GenericWorkflowWorker. service domain-name task-list)
    (.setWorkflowDefinitionFactoryFactory workflow-definition-factory-factory)))
