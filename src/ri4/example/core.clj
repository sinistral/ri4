
(ns ri4.example.core
  (:import [org.junit.runners.model
            Statement]
           [com.amazonaws.services.simpleworkflow.flow
            DecisionContextProviderImpl]
           [com.amazonaws.services.simpleworkflow.flow.junit
            GenericWorkflowTest
            WorkflowTestStatement])
  (:require [clojure.tools.logging :refer [info error]]
            [ri4.workflow-factory :refer [make-workflow-definition-factory-factory]]
            [ri4.activity-factory :refer [make-activity-implementation-factory]]
            [ri4.example.workflow :refer [+workflow-spec+]]
            [ri4.example.activity :refer [+activities+]]))

(defn run-test
  []
  (let [wff (make-workflow-definition-factory-factory [+workflow-spec+])
        wep #((:function (:entry-point +workflow-spec+)))
        activity-spec (get +activities+ "EchoActivity")
        aif (make-activity-implementation-factory [activity-spec])
        aep #((:function (:entry-point activity-spec)))]
    (.evaluate
     (doto (WorkflowTestStatement. (let [gwt (GenericWorkflowTest. wff)]
                                     (.addFactory gwt aif)
                                     (fn [] gwt))
                                   (proxy [Statement]
                                       []
                                     (evaluate [] (wep)))
                                   5000
                                   nil)
       (.setFlowTestRunner true)))))
