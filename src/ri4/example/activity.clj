
(ns ri4.example.activity
  (:require [clojure.tools.logging :refer [info error]]
            [ri4.activity :refer [defactivity make-activity-spec]]))

(defn echo
  [context]
  (info "echo says:" (.getInput (.getTask context))))

(def +activities+
  (atom {"EchoActivity" (make-activity-spec "EchoActivity"
                                            {}
                                            {:function (fn [context] (echo context))
                                             :version "v-0"})}))

(defactivity "Echo"
  {}
  {:function (fn [context] (echo context))
   :version "v-0"})
