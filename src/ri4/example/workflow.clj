
(ns ri4.example.workflow
  (:require [clojure.tools.logging :refer [info error]]
            [ri4.workflow :refer [asynchronously
                                  create-timer
                                  make-workflow-spec
                                  schedule-activity]]))

(defn delayed-echo
  []
  (info "starting workflow function")
  (let [timer (create-timer 3)]
    (info "created timer" timer)
    (asynchronously [timer]
      (schedule-activity "EchoActivity" "v-0" "hello, world")))
  (info "workflow complete"))

(def +workflow-spec+
  (make-workflow-spec "DelayedEchoWorkflow"
                      {:default-task-start-to-close-timeout 30
                       :default-execution-start-to-close-timeout 90
                       :description "workflow: delayed echo"}
                      {:function delayed-echo :version "v-0"}))
