
(ns ri4.util.bean
  (:import clojure.lang.Reflector)
  (:require [clojure.pprint :refer [cl-format]]
            [clojure.reflect :refer [reflect]]
            [clojure.string :as str]))

(defn camelcase->hyphencase
  [s]
  (let [hyphenated (str/replace (str s) #"([A-Z])" "-$1")
        leading-stripped (str/replace hyphenated #"^-" "")
        lowered (str/lower-case leading-stripped)]
    (symbol lowered)))

(defn ^:private setters
  "Returns a map whose keys are the fields in `class` as keywords, with their
names converted from \"CamelCase\" to \"hyphen-case\" and whose values are the
accessors for those fields."
  [class]
  (apply merge
         (map #(let [cc-sym (:name %)
                     hc-sym (camelcase->hyphencase cc-sym)]
                 {(keyword (str/replace-first hc-sym #"^set-" "")) (str cc-sym)})
              ;; Use :return-type to distinguish methods from fields.
              (filter #(and (:return-type %)
                            (re-find #"^set[A-Z]" (str (:name %))))
                      (:members (reflect class))))))

;; (defn hyphencase->camelcase
;;   [s]
;;   (str/join (map (str/capitalize) (str/split s #"-"))))

(defn ^:private set-field!
  [type setter value transform]
  (let [v ((or transform identity) value)]
    (when v
      (Reflector/invokeInstanceMethod type setter (to-array [v])))))

(defn default
  [d]
  (fn [v]
    (or v d)))

(defn set-fields!
  [instance value-map transformers]
  (let [type-setters (setters (class instance))]
    (loop [allowed-fields (keys type-setters)
           specified-fields (keys value-map)]
      (if (empty? allowed-fields)
        (if-not (empty? specified-fields)
          (throw (ex-info (str (cl-format nil "Invalid field(s) ~{~s~^, ~} for ~a; valid fields are ~{~s~^, ~}." (doall specified-fields) class (keys type-setters)))
                          {:fields specified-fields :class (class instance)}))
          instance)
        (let [f (first allowed-fields)]
          (set-field! instance (f type-setters) (f value-map) (f transformers))
          (recur (remove #{f} allowed-fields) (remove #{f} specified-fields))))))
  instance)
