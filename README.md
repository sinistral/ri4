# ri4

ri4 (Afrikaans "rivier"; meaning "river") is an experiment in building a
Clojure version of the AWS Simple Workflow Flow frameworks:

   http://aws.amazon.com/swf/details/flow/

It takes the pragmatic approach of building on the existing Framework for Java,
but does away with the AspectJ weaving that that framework requires.

## License

Copyright © 2014

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
