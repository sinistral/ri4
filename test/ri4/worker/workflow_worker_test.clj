
(ns ri4.worker.workflow-worker-test
  (:require [clojure.test :refer [deftest is]]
            [ri4.workflow :refer [make-workflow-spec]]
            [ri4.workflow-factory :refer [make-workflow-definition-factory-factory]])
  (:use ri4.worker.workflow-worker))

(deftest test:make-worker
  (make-worker nil
               nil
               nil
               (make-workflow-definition-factory-factory
                [(make-workflow-spec
                  "Type0"
                  {:default-task-start-to-close-timeout 30
                   :default-execution-start-to-close-timeout 90
                   :description "workflow: type-0"}
                  {:function (fn [input] (str "[0] " input))
                   :version "v-0"})
                 (make-workflow-spec
                  "Type1"
                  {:default-task-start-to-close-timeout 31
                   :default-execution-start-to-close-timeout 91
                   :description "workflow: type-1"}
                  {:function (fn [input] (str "[1] " input))
                   :version "v-1"})])))
