
(ns ri4.workflow-factory-test
  (:require [clojure.test :refer [deftest is]])
  (:require [ri4.workflow :refer [make-workflow-spec]])
  (:use ri4.workflow-factory))

(def +type-0+ (make-workflow-spec
               "Type0"
               {:default-task-start-to-close-timeout 30
                :default-execution-start-to-close-timeout 90
                :description "workflow: type-0"}
               {:function (fn [input] (str "[0] " input))
                :version "v-0"}))

(def +type-1+ (make-workflow-spec
               "Type1"
               {:default-task-start-to-close-timeout 31
                :default-execution-start-to-close-timeout 91
                :description "workflow: type-1"}
               {:function (fn [input] (str "[1] " input))
                :version "v-1"}))

(deftest test:factory-factory-types-to-poll
  (is (= [(make-workflow-type +type-1+) (make-workflow-type +type-0+)]
         (.getWorkflowTypesToRegister
          (make-workflow-definition-factory-factory [+type-0+ +type-1+])))))

(deftest test:factory-factory-get-factory
  (is (= (make-workflow-type +type-1+)
         (.getWorkflowType
          (.getWorkflowDefinitionFactory
           (make-workflow-definition-factory-factory [+type-0+ +type-1+])
           (make-workflow-type +type-1+))))))
