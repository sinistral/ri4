
(ns ri4.activity-factory-test
  (:require [clojure.test :refer [deftest is]])
  (:require [ri4.activity :refer [make-activity-spec]])
  (:use [ri4.activity-factory]))

(def +type-0+
  (make-activity-spec
   "Type-0"
   {}
   {:function (fn [context] (.getInput (.getTask context))) :version "v-0"}))

(deftest test:factory-types-to-poll
  (is (= [(make-activity-type +type-0+)]
         (.getActivityTypesToRegister
          (make-activity-implementation-factory [+type-0+])))))

(deftest test:factory-get-implementation
  (is (= (instance? com.amazonaws.services.simpleworkflow.flow.generic.ActivityImplementation
                    (.getActivityImplementation
                     (make-activity-implementation-factory [+type-0+])
                     (make-activity-type +type-0+))))))
