
(ns ri4.util.bean-test
  (import [com.amazonaws.services.simpleworkflow.flow
           WorkflowTypeRegistrationOptions])
  (:require [clojure.test :refer [deftest is]])
  (:use ri4.util.bean))

(deftest test:hyphencase->camelcase
  (is (= 'foo-bar (camelcase->hyphencase 'FooBar)))
  (is (= 'foo-bar (camelcase->hyphencase 'fooBar))))

(deftest test:setters
  (let [setters (#'ri4.util.bean/setters WorkflowTypeRegistrationOptions)]
    (is (= "setDescription" (:description setters)))
    (is (= "setDefaultTaskList" (:default-task-list setters)))
    (is (= "setDefaultChildPolicy" (:default-child-policy setters)))))

(deftest test:set-fields!
  (let [i #(com.amazonaws.services.simpleworkflow.flow.WorkflowTypeRegistrationOptions.)]
    (is (= "foo" (.getDescription (set-fields! (i) {:description "foo"} nil))))
    (is (= "foobar" (.getDescription (set-fields! (i) {:description "foo"} {:description #(str % "bar")}))))
    (is (= "default" (.getDescription (set-fields! (i) {} {:description (default "default")}))))
    (is (thrown-with-msg? RuntimeException #"Invalid field"
                          (set-fields! (i) {:foo "foo"} nil)))))
